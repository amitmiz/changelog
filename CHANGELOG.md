# [0.10.0](https://gitlab.com/amitmiz/changelog/compare/v0.9.0...v0.10.0) (2018-11-09)


### Features

* **test:** test ([2354cfd](https://gitlab.com/amitmiz/changelog/commit/2354cfd))

# [0.2.0](https://gitlab.com/amitmiz/changelog/compare/v0.1.0...v0.2.0) (2018-11-08)


### Features

* **changelog:** added changelog generator ([ab4ff2e](https://gitlab.com/amitmiz/changelog/commit/ab4ff2e))



# 0.1.0 (2018-11-08)
